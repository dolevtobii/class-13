#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include "Helper.h"
#include <exception>
#include <iostream>
#include <string>
#include <fstream>
#include <thread>
#include <queue>
#include <algorithm>
#include <condition_variable>

using std::string;
using std::cout;
using std::endl;

class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:

	void accept();
	void clientHandler(SOCKET clientSocket);
	void WriteToFile(string fileName, string user);
	string ReadFromFile(string fileName);
	void toString();
	void delName(string userName);

	std::condition_variable cv;
	std::mutex m;
	bool check;

	SOCKET _serverSocket;
};

