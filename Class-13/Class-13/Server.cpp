#include "Server.h"

string users_arr[1024];
string users = "";
int count_users = 0;
std::queue<std::string> messages;

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	std::thread client(&Server::clientHandler, this, client_socket);
	// the function that handle the conversation with the client
	//clientHandler(client_socket);
	client.detach();
}


void Server::clientHandler(SOCKET clientSocket)
{
	int type = 0;
	string msg = "";
	string file_content = "";
	string second_username = "";
	string second_user = "";
	string user = "";
	string file_name = "";
	int i = 0;
	int len = 0;
	try
	{
		//login
		type = Helper::getMessageTypeCode(clientSocket);
		len = Helper::getIntPartFromSocket(clientSocket, 2);
		user = Helper::getStringPartFromSocket(clientSocket, len);
		
		//std::cout << user << std::endl;
		users_arr[count_users] = user;
		count_users++;
		std::sort(users_arr, users_arr + count_users);
		toString();
		
		Helper::send_update_message_to_client(clientSocket, file_content, second_username, users);

		
		//update
		while (true)
		{
			Helper::getMessageTypeCode(clientSocket);
			len = Helper::getIntPartFromSocket(clientSocket, 2);
			second_user = Helper::getStringPartFromSocket(clientSocket, len);

			len = Helper::getIntPartFromSocket(clientSocket, 5);
			msg = Helper::getStringPartFromSocket(clientSocket, len);
			
			if (user < second_user)
			{
				file_name = user + "&" + second_user + ".txt";
			}
			else
			{
				file_name = second_user + "&" + user + ".txt";
			}
			
			if (second_user != "" && msg != "" )
			{
				check = true;
				m.lock();
				messages.push(msg);
				m.unlock();
				std::thread handle(&Server::WriteToFile, this, file_name, user);

				handle.join();

			}
		
			file_content = ReadFromFile(file_name);
			Helper::send_update_message_to_client(clientSocket, file_content, second_username, users);
		}
	}
	catch (const std::exception& e)
	{
		delName(user);
		closesocket(clientSocket);
	}


}

void Server::WriteToFile(string fileName, string user)
{
	std::unique_lock<std::mutex> lk(m);
	while (!check)
	{
		cv.wait(lk);
	}

	std::ofstream file_write(fileName, std::ios_base::app);
	if (file_write.is_open())
	{
		file_write << "&MAGSH_MESSAGE&&Author&" + user + "&DATA&" + messages.back();
		file_write.close();
	}
	else
	{
		cout << "Unable to open file";
	}
	check = false;
	cv.notify_all();
	/*std::queue<string> temp = messages;
	while (!temp.empty())
	{
		std::cout << temp.front() << " ";
		temp.pop();
	}
	cout << endl;*/
}

string Server::ReadFromFile(string fileName)
{
	string line = "";
	string file_content = "";
	
	std::ifstream file_read2(fileName);
	if (file_read2.is_open())
	{
		while (std::getline(file_read2, line))
		{
			file_content += line;
		}
		file_read2.close();
	}
	return file_content;
}

void Server::delName(string userName)
{
	int i = 0;
	int j = 0;
	int index = 0;
	for (i = 0; i < count_users; i++)
	{
		if (users_arr[i] == userName)
		{
			index = i;
		}
	}
	
	if (index < count_users)
	{
		count_users = count_users - 1;
		for (j = index; j < count_users; j++)
		{
			users_arr[j] = users_arr[j + 1];
		}
	}

	toString();
}

void Server::toString()
{
	int i = 0;
	users = "";
	for(i = 0; i < count_users; i++)
	{
		users += users_arr[i];
		if(i + 1 < count_users)
		{
			users += "&";
		}
	}
	
}

